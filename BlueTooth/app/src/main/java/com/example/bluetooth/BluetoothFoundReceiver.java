package com.example.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BluetoothFoundReceiver extends BroadcastReceiver {

    private BluetoothAdapter adapter = null;

    public BluetoothFoundReceiver(BluetoothAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        try {
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //TODO: 开始扫描
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //TODO: 结束扫描
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //TODO: 发现设备,每扫描到一个设备都会触发一次
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                //TODO: 得到蓝牙设备
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                //TODO: 蓝牙开关状态
                int status = adapter.getState();
                switch (status) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.e("TAG", "蓝牙状态:关闭");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.e("TAG", "蓝牙状态:打开");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.e("TAG", "蓝牙状态:正在关闭");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.e("TAG", "蓝牙状态:正在打开");
                        break;
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}