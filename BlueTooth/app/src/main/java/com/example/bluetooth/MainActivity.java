package com.example.bluetooth;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_BLUETOOTH_CONNECT = 1;
    private static final int REQUEST_BLUETOOTH_SCAN=1;
    private BluetoothManager bluetoothManager = null;
    private BluetoothAdapter bluetoothAdapter= null;

    private DeviceAdapter adapter;
    private List<BluetoothDevice> scanDevices=new ArrayList<>();

    private BluetoothFoundReceiver bluetoothFoundReceiver;
    private ActivityResultLauncher<Intent> launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnOpen=(Button) findViewById(R.id.btn_open);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenBlueTooth(view);
            }
        });

        Button btnBroadcast=(Button)findViewById(R.id.btn_broadcast);
        btnBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterBroadcast();
            }
        });

        Button btnScan=(Button) findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanDevice();
            }
        });

        RecyclerView rv_devices=findViewById(R.id.rv_device);
        rv_devices.setLayoutManager(new LinearLayoutManager(this));
        adapter=new DeviceAdapter(scanDevices);
        rv_devices.setAdapter(adapter);

        //通过Launcher来代替过期的StartActivityForResult方法
        launcher=registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if(result.getResultCode()==RESULT_OK)
                    {
                        Intent data=result.getData();
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH_CONNECT},REQUEST_BLUETOOTH_CONNECT);
                        }
                        bluetoothAdapter.enable();
                        Toast.makeText(this, "蓝牙打开成功!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH_SCAN},REQUEST_BLUETOOTH_SCAN);
        }

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "不支持蓝牙设备", Toast.LENGTH_SHORT).show();
        } else {
            bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();
            IsBlueToothOpen();
            Toast.makeText(this, "支持蓝牙设备", Toast.LENGTH_SHORT).show();
        }
    }

    //注册广播
    private void RegisterBroadcast(){
        bluetoothFoundReceiver=new BluetoothFoundReceiver(bluetoothAdapter);
        IntentFilter filter=new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);//连接蓝牙,断开蓝牙
        filter.addAction(BluetoothDevice.ACTION_FOUND);//找到设备的广播
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);//搜索完成的广播
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);//状态改变,开始配对,配对成功
        registerReceiver(bluetoothFoundReceiver,filter);
    }


    //    @SuppressLint("MissingPermission")

    //判断是否打开蓝牙,没打开则请求打开
    private void IsBlueToothOpen() {
        if (bluetoothAdapter== null || !bluetoothAdapter.isEnabled()) {
            Intent enableIntent=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            launcher.launch(enableIntent);
        }
    }

    //手动打开蓝牙
    private  void OpenBlueTooth(View view){
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.BLUETOOTH_CONNECT)!=PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this, "没有权限", Toast.LENGTH_SHORT).show();
        }else {
            bluetoothAdapter.enable();
        }
    }

    //更新列表
    private  void UpdateDeviceList(){
        adapter.notifyDataSetChanged();
    }

    //扫描蓝牙设备
    private  void ScanDevice(){
        BluetoothLeScanner scanner=bluetoothAdapter.getBluetoothLeScanner();
        ScanCallback callback=new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                BluetoothDevice device=result.getDevice();
                if(!scanDevices.contains(device))
                {
                    scanDevices.add(device);
                    UpdateDeviceList();
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                super.onScanFailed(errorCode);
                Log.e("TAG","搜索出错:"+errorCode);
            }
        };
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH_SCAN},REQUEST_BLUETOOTH_SCAN);
        }
        scanner.startScan(callback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(bluetoothFoundReceiver!=null)
        {
            unregisterReceiver(bluetoothFoundReceiver);
        }
    }
}